#include <Wire.h>
#include <RTClib.h>
#include <OneWire.h>
#include <DS1307.h>
//#include <DallasTemperature.h>         // Подключаем библиотеку с функциями для работы с DS18B20 (запросы, считывание и преобразование возвращаемых данных) 
RTC_DS1307 RTC;
//DS1307 rtc(A4, A5);
//----------Объявляем разные переменные------------
int Relay1 = 4;               //РЕЛЕ 8 PIN  4 канала         основной свет
int Relay2 = 11;              //РЕЛЕ 1 PIN 11 канала         Аэратор подстветка мощные диоды
int Relay3 = 6;               //РЕЛЕ 6 PIN  6 канала         P O M P A
int Relay4 = 7;               //РЕЛЕ 4 PIN  7 канала         POWER GLO 40 Bт
int Relay5 = 5;               //СВОБ   PIN  5 канала         Rele Free 1
int Relay6 = 9;               //РЕЛЕ 3 PIN  9 канала         Сине-зелёная подсветка в крышке
int Relay7 = 10;              //РЕЛЕ 2 PIN 10 канала         подсветка в крышке ЛИНЗЫ
int Relay8 = 2;               //РЕЛЕ 7 PIN  2 канала         подсветка синяя подстветка Тумбы
int Relay9 = 8;               //РЕЛЕ 5 PIN  8 канала         Лампа Т5
int Relay10 = 3;              //СВОБ   PIN  3 канала         Rele Free 2
int Relay11 = 12;             //СВОБ   PIN 12 канала         Rele Free 3
int Relay12 = 13;             //СВОБ   PIN 13 канала         Rele Free 4
//----------Настройки времени и продолжительности включения реле
const int DurationCh_0_01 = 60;         //ДЛИТЕЛЬНОСТЬ          1 мин    60 (в секундах)
const int DurationCh_0_30 = 1800;       //ДЛИТЕЛЬНОСТЬ         30 мин  1800 (в секундах)
const int DurationCh_0_35 = 2100;       //ДЛИТЕЛЬНОСТЬ         35 мин  2100 (в секундах)
const int DurationCh_0_45 = 2700;       //ДЛИТЕЛЬНОСТЬ         45 мин  2700 (в секундах)
const int DurationCh_1_00 = 3600;       //ДЛИТЕЛЬНОСТЬ   1 час 00 мин  3600 (в секундах)
const int DurationCh_1_30 = 5400;       //ДЛИТЕЛЬНОСТЬ   1 час 30 мин  5400 (в секундах)
const int DurationCh_1_35 = 5700;       //ДЛИТЕЛЬНОСТЬ   1 час 35 мин  5700 (в секундах)
const int DurationCh_2_00 = 7200;       //ДЛИТЕЛЬНОСТЬ   2 час 00 мин  7200 (в секундах)
const int DurationCh_2_45 = 9900;       //ДЛИТЕЛЬНОСТЬ   2 час 45 мин  9900 (в секундах)
const int DurationCh_3_00 = 10800;      //ДЛИТЕЛЬНОСТЬ   3 час 00 мин 10800 (в секундах)
const int DurationCh_4_00 = 14400;      //ДЛИТЕЛЬНОСТЬ   4 час 00 мин 14400 (в секундах)
const int DurationCh_5_10 = 18600;      //ДЛИТЕЛЬНОСТЬ   5 час 10 мин 18600 (в секундах)
const int DurationCh_6_00 = 21600;      //ДЛИТЕЛЬНОСТЬ   6 час 00 мин 21600 (в секундах)

const long StartRelCn_5_20 = 19200;
const long StartRelCn_5_25 = 19500;
const long StartRelCn_5_30 = 19800;
const long StartRelCn_6_00 = 21600;
const long StartRelCn_9_00 = 32400;
const long StartRelCn_10_30 = 37800;
const long StartRelCn_13_00 = 46800;
const long StartRelCn_19_00 = 68400;
const long StartRelCn_20_00 = 72000;

const long StartRelCn_20_10 = 72600;
const long StartRelCn_20_15 = 72900;
const long StartRelCn_20_20 = 73200;

const long StartRelCn_20_30 = 73800;
const long StartRelCn_21_00 = 75600;
const long StartRelCn_21_30 = 77400;
const long StartRelCn_22_30 = 81000;
const long StartRelCn_23_00 = 82800;
const long StartRelCn_0_00 = 0;

void setup() {
  Serial.begin(9600);                 // Запускаем вывод данных на серийный порт
  Wire.begin();
  RTC.begin();                        //Инициирум RTC модуль

  pinMode(Relay1, OUTPUT);            //Инициализируем порт 1     РЕЛЕ 8           PIN 4
  pinMode(Relay2, OUTPUT);            //Инициализируем порт 2     РЕЛЕ 1           PIN 11
  pinMode(Relay3, OUTPUT);            //Инициализируем порт 3     РЕЛЕ 6           PIN 6
  pinMode(Relay4, OUTPUT);            //Инициализируем порт 4     РЕЛЕ 4           PIN 7
//  pinMode(Relay5, OUTPUT);            //Инициализируем порт 5     Свободный        PIN 5
  pinMode(Relay6, OUTPUT);            //Инициализируем порт 6     РЕЛЕ 3           PIN 9
  pinMode(Relay7, OUTPUT);            //Инициализируем порт 7     РЕЛЕ 2           PIN 10
  pinMode(Relay8, OUTPUT);            //Инициализируем порт 8     РЕЛЕ 7           PIN 2
  pinMode(Relay9, OUTPUT);            //Инициализируем порт 9     РЕЛЕ 5           PIN 8
//  pinMode(Relay10, OUTPUT);           //Инициализируем порт 10    Свободный           PIN 3
//  pinMode(Relay11, OUTPUT);           //Инициализируем порт 11    Свободный        PIN 12
//  pinMode(Relay12, OUTPUT);           //Инициализируем порт 12    Свободный        PIN 13

  digitalWrite(Relay1, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay2, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay3, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay4, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
//  digitalWrite(Relay5, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay6, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay7, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay8, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
  digitalWrite(Relay9, HIGH);         //Устанавливаем ВЫСОКИЙ уровень
//  digitalWrite(Relay10, HIGH);        //Устанавливаем ВЫСОКИЙ уровень
//  digitalWrite(Relay11, HIGH);        //Устанавливаем ВЫСОКИЙ уровень
//  digitalWrite(Relay12, HIGH);        //Устанавливаем ВЫСОКИЙ уровень
   }
void loop()                            // ПРОГРАММЫй безусловный ЦИКЛ
   {
// Определяем время
  DateTime now = RTC.now();
// Выводим время в монитор порта
//  Serial.print(now.year(), DEC); Serial.print('/'); Serial.print(now.month(), DEC); Serial.print('/'); Serial.print(now.day(), DEC); Serial.print(' ');
//  Serial.print(now.hour(), DEC); Serial.print(':'); Serial.print(now.minute(), DEC); Serial.print(':'); Serial.print(now.second(), DEC); Serial.print(" den nedeli "); Serial.print(now.dayOfЕруWeek(), DEC);
//  Serial.println();
//  delay(3000);
//  Serial.println();
  DateTime myTime = RTC.now();       //Читаем данные времени из RTC при каждом выполнении цикла
  Serial.print(myTime.year());

  //----------Раздел обработки реле по времени ----
  long utime = myTime.unixtime();    //сохраняем в переменную - время в формате UNIX
  utime %= 86400;                    //Сохраняем в этой же переменной остаток деления на кол-во секнд в сутках. Это дает количество секунд с начала текущих суток
 //Определям день недели - БУДНИЕ ДНИ -- ВЫХОДНЫЕ ДНИ -----------------------------
  if ((now.dayOfTheWeek() == 1) || (now.dayOfTheWeek() == 2) || (now.dayOfTheWeek() == 3) || (now.dayOfTheWeek() == 4) || (now.dayOfTheWeek() == 5))
    {
    Serial.print(" grafik budnii dni "); //график будней дней
    Serial.println();
    //------------КАНАЛ 1 основной свет - БУДНИЕ ДНИ -----------------------------
    if ((utime >= StartRelCn_13_00) && (utime < (StartRelCn_13_00 + DurationCh_6_00)))
   //                        13-00                                            19-00                        
    {
      digitalWrite(Relay1, LOW);      //Устанавливаем на 1 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 1 - Svet - ventilytor - Klapan CO2"); Serial.println();
    }
    else                                              
    {
      digitalWrite(Relay1, HIGH);     //Устанавливаем на 1 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 2  синяя подстветка Тумбы  - БУДНИЕ ДНИ -----------
    if ((utime >= StartRelCn_5_25) && (utime < (StartRelCn_5_25 + DurationCh_1_00)) ||  (utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_2_00)))
    //                       5-25                                            6-25                            19-00                                            21-00              
    {
      digitalWrite(Relay2, LOW);      //Устанавливаем на 2 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 2 - cvet tumba"); Serial.println();
    }
    else
    {
      digitalWrite(Relay2, HIGH);     //Устанавливаем на 2 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 3 помпа  - БУДНИЕ ДНИ -----------
    if ((utime >= StartRelCn_5_20) && (utime < (StartRelCn_5_20 + DurationCh_5_10)) || (utime >= StartRelCn_10_30) && (utime < (StartRelCn_10_30 + DurationCh_2_45)) || (utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_1_00)) || (utime >= StartRelCn_21_00) && (utime < (StartRelCn_21_00 + DurationCh_1_00)) || (utime >= StartRelCn_23_00) && (utime < (StartRelCn_23_00 + DurationCh_0_45)))
      //                     5-20                                           10-30                           10-30                                            13-15                           19-00                                            20-00                           21-00                                            22-00                           23-00                                            23-45
    {
      digitalWrite(Relay3, LOW);      //Устанавливаем на 3 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 3  P O M P A"); Serial.println();
    }
    else
    {
      digitalWrite(Relay3, HIGH);     //Устанавливаем на 3 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 4 - Лампа POWER GLO 40 Bт - БУДНИЕ ДНИ -----------
    if ((utime >= StartRelCn_9_00) && (utime < (StartRelCn_9_00 + DurationCh_4_00)) || (utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_1_00)))
    //                       9-00                                           13-00                           19-00                                            20-00      
    {
      digitalWrite(Relay4, LOW);      //Устанавливаем на 4 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 4 - POWER GLO"); Serial.println();
    }
    else
    {
      digitalWrite(Relay4, HIGH);     //Устанавливаем на 4 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 5 - Лампа Т5  - БУДНИЕ ДН -----------//----------9 канал----------------------------
    if ((utime >= StartRelCn_20_00) && (utime < (StartRelCn_20_00 + DurationCh_0_30)) || (utime >= StartRelCn_21_30) && (utime < (StartRelCn_21_30 + DurationCh_0_30)))
    //                       20-00                                            20-30                           21-30                                            22-00
    {  
      digitalWrite(Relay9, LOW);
      Serial.print(" Rele 5  LAMPA T5"); Serial.println();
    }
    else
    {
      digitalWrite(Relay9, HIGH);     //Устанавливаем на 6 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
          //------------КАНАЛ 6 - Сине-зелёная подсветка в крышке - БУДНИЕ ДНИ -----------
    if ((utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_1_00)) || (utime >= StartRelCn_20_30) && (utime < (StartRelCn_20_30 + DurationCh_2_00)))
   //                        19-00                                            20-00                           20-30                                            22-30
    {
      digitalWrite(Relay6, LOW);      //Устанавливаем на 6 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 6 - Podsvetka v kryshke"); Serial.println();
    }
    else
    {
      digitalWrite(Relay6, HIGH);     //Устанавливаем на 6 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 7 - Сине-зелёная подсветка в крышке ЛИНЗЫ - БУДНИЕ ДНИ -----------
    if ((utime >= StartRelCn_6_00) && (utime < (StartRelCn_6_00 + DurationCh_3_00))  || (utime >= StartRelCn_20_00) && (utime < (StartRelCn_20_00 + DurationCh_1_30)) || (utime >= StartRelCn_22_30) && (utime < (StartRelCn_22_30 + DurationCh_0_45)))
     //                      6-00                                            9-00                            20-00                                            21-30                           22-30                                            23-15
    {
      digitalWrite(Relay7, LOW);      //Устанавливаем на 7 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 7 Podsvetka Linza"); Serial.println();
    }
    else
    {
      digitalWrite(Relay7, HIGH);     //Устанавливаем на 7 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 8  Аэратор подстветка мощные диоды  -- БУДНИЕ ДНИ -----------
    if ((utime >= StartRelCn_5_30) && (utime < (StartRelCn_5_30 + DurationCh_1_30)) || (utime >= StartRelCn_21_30) && (utime < (StartRelCn_21_30 + DurationCh_1_00)) || (utime >= StartRelCn_23_00) && (utime < (StartRelCn_23_00 + DurationCh_1_00)) || (utime >= StartRelCn_0_00) && (utime < (StartRelCn_0_00 + DurationCh_0_30)))
    //                       5-30                                            7-00                           21-30                                            22-30                           23-00                                            24-00                          00-00                                            00-30       
    {
      digitalWrite(Relay8, LOW);      //Устанавливаем на 8 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 8 - Hydor Ario 2 svet"); Serial.println();
    }
    else
    {
      digitalWrite(Relay8, HIGH);     //Устанавливаем на 8 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    }
//==================================================================================================================  
  else
    {
    Serial.print(" Grafik po vihodnye dni ");  //График выходных дней
    Serial.println();
    //------------КАНАЛ 1 основной свет вентилятор клапан СО2  выходные дни ------------------------------
    if ((utime >= StartRelCn_13_00) && (utime < (StartRelCn_13_00 + DurationCh_6_00)))
 //                          13-00                                            19-00                        
    {
      digitalWrite(Relay1, LOW);      //Устанавливаем на 1 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 1 - Svet - ventilytor - Klapan CO2 vihodnye dni "); Serial.println();
    }
    else                                              
    {
      digitalWrite(Relay1, HIGH);     //Устанавливаем на 1 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 2  синяя подстветка Тумбы  -  выходные дни  -----------
    if ((utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_2_00)))
      //                     19-00                                            21-00         
    {
      digitalWrite(Relay2, LOW);      //Устанавливаем на 2 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 2 - cvet tumba  vihodnye dni "); Serial.println();
    }
    else
    {
      digitalWrite(Relay2, HIGH);     //Устанавливаем на 2 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 3 - помпа большой аквас - компрессор воздух БОЛ -  выходные дни -----------
    if ((utime >= StartRelCn_10_30) && (utime < (StartRelCn_10_30 + DurationCh_2_45)) || (utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_1_00)) || (utime >= StartRelCn_21_00) && (utime < (StartRelCn_21_00 + DurationCh_1_00)) || (utime >= StartRelCn_23_00) && (utime < (StartRelCn_23_00 + DurationCh_0_45)))
      //                     10-30                                            13-15                           19-00                                            19-30                           21-00                                            22-00                           23-00                                            23-45
    {
      digitalWrite(Relay3, LOW);      //Устанавливаем на 3 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 3 -  P O M P A vihodnye dni "); Serial.println();
    }
    else
    {
      digitalWrite(Relay3, HIGH);     //Устанавливаем на 3 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 4 - Лампа POWER GLO 40 Bт выходные дни -----------
     if ((utime >= StartRelCn_9_00) && (utime < (StartRelCn_9_00 + DurationCh_4_00)) || (utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_0_30)))
    //                        9-00                                           13-00                           19-00                                            19-30      
    {
      digitalWrite(Relay4, LOW);      //Устанавливаем на 4 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 4 - POWER GLO vihodnye dni "); Serial.println();
    }
    else
    {
      digitalWrite(Relay4, HIGH);     //Устанавливаем на 4 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 5 - Лампа Т5  - выходные дни -----------
    if ((utime >= StartRelCn_20_00) && (utime < (StartRelCn_20_00 + DurationCh_0_30)) || (utime >= StartRelCn_21_30) && (utime < (StartRelCn_21_30 + DurationCh_0_30)))
    //                       20-00                                            20-30                           21-30                                            22-00
    {  
      digitalWrite(Relay9, LOW);
      Serial.print(" Rele 5 LAMPA T5"); Serial.println();
    }
    else
    {
      digitalWrite(Relay9, HIGH);     //Устанавливаем на 6 входе релейного модуля ВЫСОКИЙ уровень - реле выключается 74700
    }
     //------------КАНАЛ 6 - Сине-зелёная подсветка в крышке  выходные дни -----------
     if ((utime >= StartRelCn_19_00) && (utime < (StartRelCn_19_00 + DurationCh_1_00)) || (utime >= StartRelCn_20_30) && (utime < (StartRelCn_20_30 + DurationCh_2_00)))
   //                         19-00                                            20-00                           20-30                                            22-30
    
    {
      digitalWrite(Relay6, LOW);     //Устанавливаем на 6 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 6 - Podsvetka v kryshke vihodnye dni "); Serial.println();
    }
    else
    {
      digitalWrite(Relay6, HIGH);    //Устанавливаем на 6 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 7 - Сине-зелёная подсветка в крышке ЛИНЗЫ  выходные дни -----------
    if ((utime >= StartRelCn_20_00) && (utime < (StartRelCn_20_00 + DurationCh_1_30)) || (utime >= StartRelCn_22_30) && (utime < (StartRelCn_22_30 + DurationCh_0_45)))
    //                       20-00                                        21-30                               22-30                                            23-15      
    {
      digitalWrite(Relay7, LOW);     //Устанавливаем на 7 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 7_1  Podsvetka Linza vihodnye dni "); Serial.println();
    }
    else
    {
      digitalWrite(Relay7, HIGH);     //Устанавливаем на 7 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    //------------КАНАЛ 8 - Аэратор Hydor "Ario 2" подсветка  мощные диоды    выходные дни -----------
    if ((utime >= StartRelCn_21_30) && (utime < (StartRelCn_21_30 + DurationCh_1_00)) || (utime >= StartRelCn_23_00) && (utime < (StartRelCn_23_00 + DurationCh_1_00)) || (utime >= StartRelCn_0_00) && (utime < (StartRelCn_0_00 + DurationCh_0_30)))
//                           21-30                                            22-30                           23-00                                            24-00                          00-00                                           00-30
    {
      digitalWrite(Relay8, LOW);      //Устанавливаем на 8 входе релейного модуля НИЗКИЙ уровень - реле срабатывает
      Serial.print(" Rele 8 - Hydor Ario 2 svet vihodnye dni "); Serial.println();
    }
    else
    {
      digitalWrite(Relay8, HIGH);     //Устанавливаем на 8 входе релейного модуля ВЫСОКИЙ уровень - реле выключается
    }
    }
    }//------------Конец ЦИКЛА-----------------------------
