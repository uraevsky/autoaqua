#include <Wire.h>
#include <RTClib.h>
#include <OneWire.h>
#include <DS1307.h>
//#include <DallasTemperature.h>         // Подключаем библиотеку с функциями для работы с DS18B20 (запросы, считывание и преобразование возвращаемых данных) 
RTC_DS1307 RTC;
//DS1307 rtc(A4, A5);
//----------Объявляем разные переменные------------
int relay[]={
  4,    //РЕЛЕ 8 PIN  4 канала         Основной свет
  11,   //РЕЛЕ 1 PIN  11 канала        Аэратор подстветка мощные диоды
  6,    //РЕЛЕ 6 PIN  6 канала         Помпа
  7,    //РЕЛЕ 4 PIN  7 канала         POWER GLO 40 Bт
  9,    //РЕЛЕ 3 PIN  9 канала         Сине-зелёная подсветка в крышке 
  10,   //РЕЛЕ 2 PIN 10 канала         Подсветка в крышке ЛИНЗЫ
  2,    //РЕЛЕ 7 PIN  2 канала         Синяя подстветка Тумбы
  8     //РЕЛЕ 5 PIN  8 канала         Лампа Т5
};

int numbport = 8;
//----------Объявляем график включения Будние дни------------
const int weekday[][8] = {
  {13, 0, 19, 0,  0, 0,  0, 0},   // Основной свет
  { 5,30,  7, 0, 22, 0, 23,30},   // Аэратор подстветка мощные диоды
  { 5,20, 13,15, 19, 0, 21, 0},   // Помпа
  { 9, 0, 13, 0, 19, 0, 20, 0},   // POWER GLO 40 Bт
  {19, 0, 20, 0, 20,30, 22,30},   // Сине-зелёная подсветка в крышке
  { 6, 0,  9, 0, 21, 0, 23, 0},   // Подсветка в крышке ЛИНЗЫ
  { 5,25,  6,25, 19, 0, 21, 0},   // Cиняя подстветка Тумбы
  {20, 0, 20,30, 21,30, 22, 0}    // Лампа Т5
};
//----------Объявляем график включения Выходные дни------------
const int holiday[][8] = {
  {13, 0, 19, 0,  0, 0,  0, 0},   // Основной свет
  { 5,30,  7, 0, 22, 0, 23,30},   // Аэратор подстветка мощные диоды
  { 5,20, 13,15, 19, 0, 21, 0},   // Помпа
  { 9, 0, 13, 0, 19, 0, 20, 0},   // POWER GLO 40 Bт
  {19, 0, 20, 0, 20,30, 22,30},   // Сине-зелёная подсветка в крышке
  { 6, 0,  9, 0, 21, 0, 23, 0},   // Подсветка в крышке ЛИНЗЫ
  { 5,25,  6,25, 19, 0, 21, 0},   // Cиняя подстветка Тумбы
  {20, 0, 20,30, 21,30, 22, 0}    // Лампа Т5
};
          
//----------Настройки времени и продолжительности включения реле
const int MINUTE = 60;
const int HOUR = 3600;

void setup() {
// Запускаем вывод данных на серийный порт
  Serial.begin(9600);
  Wire.begin();
//Инициирум RTC модуль
  RTC.begin();

//Инициализируем порты и устанавливаем ВЫСОКИЙ уровень портов
  for (int i = 0; i <= sizeof(relay); i++) {
    pinMode(relay[i], OUTPUT);
    digitalWrite(relay[i], HIGH);
  }
}

void loop() {
// Определяем время
  DateTime now = RTC.now();   //Читаем данные времени из RTC при каждом выполнении цикла
  Serial.print(now.day());Serial.print(now.month());Serial.print(now.year());

  //----------Раздел обработки реле по времени ----
  long utime = now.unixtime();    //Cохраняем в переменную - время в формате UNIX
  utime %= 86400;                 //Сохраняем в этой же переменной остаток деления на кол-во секнд в сутках. Это дает количество секунд с начала текущих суток
//  Определям день недели
//  --- БУДНИЕ ДНИ ---
  if ((now.dayOfTheWeek() <= 5)) {
    Serial.print(" Weekdays plan "); // График будних дней
    Serial.println();
    for (int p = 1; p <= numbport; p++) {
      Serial.print("Setting "); Serial.println(p); Serial.print(" port");
      if ((utime >= (weekday[p][1] + weekday[p][2])) && (utime < (weekday[p][3] + weekday[p][4])) || (utime >= (weekday[p][5] + weekday[p][6])) && (utime < (weekday[p][7] + weekday[p][8]))) {
         digitalWrite(relay[(p-1)], HIGH);
      }
      else {
        digitalWrite(relay[(p-1)], LOW);
      }
    }
  }
//  --- ВЫХОДНЫЕ ДНИ ---  
  if ((now.dayOfTheWeek() > 5)) {
    Serial.print(" Weekdays plan "); // График будних дней
    Serial.println();
    for (int p = 1; p <= numbport; p++) {
      Serial.print("Setting "); Serial.println(p); Serial.print(" port");
      if ((utime >= (holiday[p][1] + holiday[p][2])) && (utime < (holiday[p][3] + holiday[p][4])) || (utime >= (holiday[p][5] + holiday[p][6])) && (utime < (holiday[p][7] + holiday[p][8]))) {
         digitalWrite(relay[(p-1)], HIGH);
      }
      else {
        digitalWrite(relay[(p-1)], LOW);
      }
    }
  }
}
